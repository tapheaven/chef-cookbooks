# encoding: UTF-8
#
# => mod_cluster Configuration

# => Source
default['apache']['mod_cluster']['version'] = '1.2.6.Final'
default['apache']['mod_cluster']['url'] = 'http://downloads.jboss.org/mod_cluster/1.2.6.Final/linux-x86_64/mod_cluster-1.2.6.Final-linux2-x64-so.tar.gz'
default['apache']['mod_cluster']['checksum'] = 'c3e5412eb2fa24b8daca9e1ff48654e7'

# => Base Directory
default['apache']['base'] = '/etc/httpd'

# => Set Apache User & Group
default['apache']['user'] = 'apache'
default['apache']['group'] = 'apache'

# => Set Apache Service Name
default['apache']['service'] = 'httpd'

# => Enforce Configuration (Force's redeployment of configuration, overwriting any local changes)
default['apache']['enforce_config'] = false

# => httpd Configuration
default['apache']['ip'] = node['ipaddress']
default['apache']['port'] = '80'
default['apache']['mod_cluster']['port'] = '10001'
default['apache']['mod_cluster']['allow_address'] = default['apache']['ip'][0..default['apache']['ip'].index('.', default['apache']['ip'].index('.') + 1)]