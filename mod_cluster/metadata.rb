name             'mod_cluster'
maintainer       'Brian Krebs'
maintainer_email 'bkrebs@tapheaven.com'
license          'All rights reserved'
description      'Installs/Configures Apache httpd with mod_cluster'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
