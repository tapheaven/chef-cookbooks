#
# Cookbook Name:: mod_cluster
# Recipe:: default
#
# Copyright 2014, TapHeaven
#
# All rights reserved - Do Not Redistribute
#

# => Shorten Hashes
apache = node['apache']
mod_cluster = node['apache']['mod_cluster']

# => Update System
#include_recipe 'apt' if platform?('ubuntu', 'debian')
#include_recipe 'yum' if platform_family?('rhel')

# => Create Apache System User
user apache['user'] do
  comment 'Apache'
  shell '/bin/bash'
  supports manage_home: true
  action [:create, :lock]
end

# => Create Apache Group
group apache['group'] do
  append true
  members apache['user']
  action :create
  only_if { apache['user'] != apache['group'] }
end

# => Install httpd
package 'httpd' do
  action :install
end

mod_cluster_file = "mod_cluster-#{mod_cluster['version']}.tar.gz"

# => Download mod_cluster Tarball
remote_file "#{Chef::Config[:file_cache_path]}/#{mod_cluster_file}" do
  source mod_cluster['url']
  checksum mod_cluster['checksum']
  action :create
  notifies :run, 'bash[Extract mod_cluster]', :immediately
end

# => Extract mod_cluster
bash 'Extract mod_cluster' do
  cwd Chef::Config[:file_cache_path]
  code <<-EOF
  tar xzf #{mod_cluster_file} -C #{apache['base']}/modules
  chown #{apache['user']}:#{apache['group']} -R #{apache['base']}
  EOF
  action :nothing
end

# => Configure httpd
template File.join(apache['base'], 'conf', 'httpd.conf') do
  source 'httpd.conf.erb'
  user apache['user']
  group apache['group']
  mode '0644'
  variables({
    ip: apache['ip'],
    port: apache['port'],
    cluster_manager_port: mod_cluster['port'],
    allow_address: mod_cluster['allow_address'],
    user: apache['user'],
    group: apache['group']
  })
  notifies :restart, "service[#{apache['service']}]", :delayed
  only_if { !File.exists?(File.join(apache['base'], '.chef_deployed')) || apache['enforce_config'] }
end

# Create file to indicate deployment and prevent recurring configuration deployment
file File.join(apache['base'], '.chef_deployed') do
  owner apache['user']
  group apache['group']
  action :create_if_missing
end

# => Start the httpd Service
service apache['service'] do
  action :start
end