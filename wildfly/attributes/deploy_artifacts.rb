# encoding: UTF-8
#
# => Wildfly Deployments
# => Stored as an array of hashes

default['wildfly']['deploy']['artifacts'] = [
	{
		name: 'postgresql-9.3-1101.jdbc41.jar',
		s3_bucket: 'qa.com.tapheaven.build.artifacts',
		s3_path: 'postgresql-9.3-1101.jdbc41.jar',
		server_groups: 'dsp-service\,dsp-service-backup'
	},
	{
		name: 'm.war',
		s3_bucket: 'qa.com.tapheaven.build.artifacts',
		s3_path: 'm.war',
		server_groups: 'dsp-service\,dsp-service-backup'
	},
	{
		name: 'rtb.war',
		s3_bucket: 'qa.com.tapheaven.build.artifacts',
		s3_path: 'rtb.war',
		server_groups: 'dsp-service\,dsp-service-backup'
	},
	{
		name: 'data.war',
		s3_bucket: 'qa.com.tapheaven.build.artifacts',
		s3_path: 'data.war',
		server_groups: 'dsp-service\,dsp-service-backup'
	},
	{
		name: 'analytics.war',
		s3_bucket: 'qa.com.tapheaven.build.artifacts',
		s3_path: 'analytics.war',
		server_groups: 'dsp-service\,dsp-service-backup'
	}
]
