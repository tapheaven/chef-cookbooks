# encoding: UTF-8
# => Wildfly User Configuration

# => Access Control Provider (simple, or rbac)
default['wildfly']['acp'] = 'simple'

# => By default, Wildfly expects this password hash format:
# => # => username=HEX( MD5( username ':' realm ':' password))

# => Default user - wildfly - wildfly
default['wildfly']['users']['mgmt'] = [
  { id: 'wildfly', passhash: '2c6368f4996288fcc621c5355d3e39b7' },
  { id: 'slave_fresno', passhash: '3481a53a1bea014b3efb17ac4f18920e' },
  { id: 'slave_denver', passhash: 'b1ad257724911af447a2f50a9713500f' },
  { id: 'slave_kansas', passhash: '6eb255fcc9e70b1146e48da7a88a0ebf' },
  { id: 'slave_wichita', passhash: '9f1770567532bdd27e2221354f849c73' },
  { id: 'slave_corpus-christi', passhash: 'ce7e62f911af09040c51aadbd7492646' },
  { id: 'slave_dallas', passhash: '506e4c795640882b1e90a64ba57ba456' },
  { id: 'slave_houston', passhash: '64c9916421697c1ad653b7995bd3e1a4' },
  { id: 'slave_long-beach', passhash: '3bd18a0dd3ce93126b7ad9e28e68c98a' },
  { id: 'slave_norfolk', passhash: '1a733af727238230f842cae87723451d' }
]
