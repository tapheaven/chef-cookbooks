# encoding: UTF-8
#
# => Custom Wildfly Configuration

# => Datasources
default['wildfly']['custom']['datasource']['business']['url'] = 'jdbc:postgresql://db.qa.tapheaven.net:5432/tapheaven'
default['wildfly']['custom']['datasource']['business']['driver_class'] = 'org.postgresql.Driver'
default['wildfly']['custom']['datasource']['business']['driver'] = 'postgresql-9.3-1101.jdbc41.jar'
default['wildfly']['custom']['datasource']['business']['username'] = 'tapheaven'
default['wildfly']['custom']['datasource']['business']['password'] = 'Qu4cK123#'
default['wildfly']['custom']['datasource']['business']['pool']['min'] = '1'
default['wildfly']['custom']['datasource']['business']['pool']['max'] = '20'

default['wildfly']['custom']['datasource']['analysis']['url'] = 'jdbc:postgresql://db.analysis.qa.tapheaven.net:5432/tapheaven'
default['wildfly']['custom']['datasource']['analysis']['driver_class'] = 'org.postgresql.Driver'
default['wildfly']['custom']['datasource']['analysis']['driver'] = 'postgresql-9.3-1101.jdbc41.jar'
default['wildfly']['custom']['datasource']['analysis']['username'] = 'tapheaven'
default['wildfly']['custom']['datasource']['analysis']['password'] = 'Qu4cK123#'
default['wildfly']['custom']['datasource']['analysis']['pool']['min'] = '10'
default['wildfly']['custom']['datasource']['analysis']['pool']['max'] = '50'

# => Messaging
default['wildfly']['custom']['messaging']['cluster_password'] = 'Qu4cK123#'
default['wildfly']['custom']['messaging']['redelivery_delay'] = '200'
default['wildfly']['custom']['messaging']['max_delivery_attempts'] = '5'

# => Server Groups
default['wildfly']['custom']['server_groups'] = [
	{
		name: 'dsp-service',
		profile: 'full-ha',
		heap_size: '64m',
		heap_max: '512m',
		socket_binding: 'full-ha-sockets'
	},
	{
		name: 'dsp-service-backup',
		profile: 'full-ha',
		heap_size: '64m',
		heap_max: '512m',
		socket_binding: 'full-ha-sockets'
	}
]

# => Servers
default['wildfly']['custom']['servers'] = [
	{
		name: 'dsp-service-alpha',
		group: 'dsp-service',
		auto_start: true,
		port_offset: 0
	},
	{
		name: 'dsp-service-beta',
		group: 'dsp-service',
		auto_start: true,
		port_offset: 150
	},
	{
		name: 'dsp-service-gamma',
		group: 'dsp-service',
		auto_start: true,
		port_offset: 250
	}
]

# => Amazon OpsWorks Layers
default['wildfly']['custom']['opsworks']['layer']['proxy'] = 'apache_lb_qa'
default['wildfly']['custom']['opsworks']['layer']['master'] = 'wildfly8_dc_qa'

# => mod_cluster Proxy
default['wildfly']['custom']['proxy']['ip'] = '0.0.0.0'
default['wildfly']['custom']['proxy']['port'] = '10001'

# => Slave Configuration
default['wildfly']['custom']['slave']['password'] = 'wildfly'
default['wildfly']['custom']['slave']['secret'] = 'd2lsZGZseQ=='

# => For Testing Outside of AWS
default['opsworks']['layers']['apache_lb_qa']['instances']['dionysus']['private_ip'] = '172.31.18.138'
default['opsworks']['layers']['wildfly8_dc_qa']['instances']['poseidon']['private_ip'] = '172.31.29.203'