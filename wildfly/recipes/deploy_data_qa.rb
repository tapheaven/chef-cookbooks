# encoding: UTF-8
#
# Cookbook Name:: wildfly
# Recipe:: deploy_artifacts
#
# Copyright (C) 2014 Brian Dwyer - Intelligent Digital Services
# 
# All rights reserved - Do Not Redistribute
#

node.override['wildfly']['deploy']['artifacts'] = [
	{
		name: 'postgresql-9.3-1101.jdbc41.jar',
		s3_bucket: 'qa.com.tapheaven.build.artifacts',
		s3_path: 'postgresql-9.3-1101.jdbc41.jar',
		server_groups: 'dsp-service\,dsp-service-backup'
	},
	{
		name: 'data.war',
		s3_bucket: 'qa.com.tapheaven.build.artifacts',
		s3_path: 'data.war',
		server_groups: 'dsp-service\,dsp-service-backup'
	}
]

include_recipe 'wildfly::deploy_artifacts'