# encoding: UTF-8
#
# Cookbook Name:: wildfly
# Recipe:: install
#
# Copyright (C) 2014 Brian Dwyer - Intelligent Digital Services
#
# All rights reserved - Do Not Redistribute
#

# => Update System
include_recipe 'apt' if platform?('ubuntu', 'debian')
include_recipe 'yum' if platform_family?('rhel')

# => Create Wildfly System User
user node['wildfly']['user'] do
  comment 'Wildfly'
  shell '/bin/bash'
  supports manage_home: true
  action [:create, :lock]
end

# => Create Wildfly Group
group node['wildfly']['group'] do
  append true
  members node['wildfly']['user']
  action :create
  only_if { node['wildfly']['user'] != node['wildfly']['group'] }
end

# => Create Wildfly Directory
directory node['wildfly']['base'] do
  owner node['wildfly']['user']
  group node['wildfly']['group']
  mode 0755
  recursive true
end

# => Ensure LibAIO Present for Java NIO Journal
case node[:platform_family]
when 'rhel'
  package 'libaio' do
    action :install
  end
when 'debian'
  package 'libaio1' do
    action :install
  end
end

# => Download Wildfly Tarball
remote_file "#{Chef::Config[:file_cache_path]}/#{node['wildfly']['version']}.tar.gz" do
  source node['wildfly']['url']
  checksum node['wildfly']['checksum']
  action :create
  notifies :run, 'bash[Extract Wildfly]', :immediately
end

# => Extract Wildfly
bash 'Extract Wildfly' do
  cwd Chef::Config[:file_cache_path]
  code <<-EOF
  tar xzf #{node['wildfly']['version']}.tar.gz -C #{node['wildfly']['base']} --strip 1
  chown #{node['wildfly']['user']}:#{node['wildfly']['group']} -R #{node['wildfly']['base']}
  EOF
  action :nothing
end

# Deploy Init Script
template File.join('etc', 'init.d', node['wildfly']['service']) do
  source 'wildfly-init-redhat.sh.erb'
  user 'root'
  group 'root'
  mode '0755'
end

# Deploy Service Configuration
template File.join('etc', 'default', 'wildfly.conf') do
  source 'wildfly.conf.erb'
  user 'root'
  group 'root'
  mode '0644'
end

# => Set Interfaces
if "0.0.0.0" == node['wildfly']['int']['mgmt']['bind']
	node.override['wildfly']['int']['mgmt']['bind'] = node['ipaddress']
end

if "0.0.0.0" == node['wildfly']['int']['pub']['bind']
	node.override['wildfly']['int']['pub']['bind'] = node['ipaddress']
end

# => Configure Wildfly Domain - Interfaces
template File.join(node['wildfly']['base'], 'domain', 'configuration', node['wildfly']['dom']['conf']) do
  source "#{node['wildfly']['dom']['conf']}.erb"
  user node['wildfly']['user']
  group node['wildfly']['group']
  mode '0644'
  variables({
    pub_http_port: node['wildfly']['int']['pub']['http_port'],
    pub_https_port: node['wildfly']['int']['pub']['https_port'],
    wsdl_int: node['wildfly']['int']['wsdl']['bind'],
    ajp_port: node['wildfly']['int']['ajp']['port'],
    smtp_host: node['wildfly']['smtp']['host'],
    smtp_port: node['wildfly']['smtp']['port'],
    smtp_ssl: node['wildfly']['smtp']['ssl'],
    smtp_user: node['wildfly']['smtp']['username'],
    smtp_pass: node['wildfly']['smtp']['password'],
    acp: node['wildfly']['acp'],
    s3_access_key: node['wildfly']['aws']['s3_access_key'],
    s3_secret_access_key: node['wildfly']['aws']['s3_secret_access_key'],
    s3_bucket: node['wildfly']['aws']['s3_bucket'],
    business_datasource_url: node['wildfly']['custom']['datasource']['business']['url'],
    business_datasource_driver_class: node['wildfly']['custom']['datasource']['business']['driver_class'],
    business_datasource_driver: node['wildfly']['custom']['datasource']['business']['driver'],
    business_datasource_username: node['wildfly']['custom']['datasource']['business']['username'],
    business_datasource_password: node['wildfly']['custom']['datasource']['business']['password'],
    business_datasource_min_pool_size: node['wildfly']['custom']['datasource']['business']['pool']['min'],
    business_datasource_max_pool_size: node['wildfly']['custom']['datasource']['business']['pool']['max'],
    analysis_datasource_url: node['wildfly']['custom']['datasource']['analysis']['url'],
    analysis_datasource_driver_class: node['wildfly']['custom']['datasource']['analysis']['driver_class'],
    analysis_datasource_driver: node['wildfly']['custom']['datasource']['analysis']['driver'],
    analysis_datasource_username: node['wildfly']['custom']['datasource']['analysis']['username'],
    analysis_datasource_password: node['wildfly']['custom']['datasource']['analysis']['password'],
    analysis_datasource_min_pool_size: node['wildfly']['custom']['datasource']['analysis']['pool']['min'],
    analysis_datasource_max_pool_size: node['wildfly']['custom']['datasource']['analysis']['pool']['max'],
    messaging_cluster_password: node['wildfly']['custom']['messaging']['cluster_password'],
    messaging_redelivery_delay: node['wildfly']['custom']['messaging']['redelivery_delay'],
    messaging_max_delivery_attempts: node['wildfly']['custom']['messaging']['max_delivery_attempts'],
    proxy_ip: node['wildfly']['custom']['proxy']['ip'],
    proxy_port: node['wildfly']['custom']['proxy']['port'],
    transaction_timeout: node['wildfly']['dom']['transaction']['timeout'],
    max_ejb_threads: node['wildfly']['dom']['ejb']['max_threads'],
    max_io_threads: node['wildfly']['dom']['io']['max_threads']
  })
  notifies :restart, "service[#{node['wildfly']['service']}]", :delayed
  only_if { !node['wildfly']['dom']['host']['slave'] && (!File.exists?(File.join(node['wildfly']['base'], '.chef_deployed')) || node['wildfly']['enforce_config']) }
end

# => Configure Wildfly Host - Interfaces
template File.join(node['wildfly']['base'], 'domain', 'configuration', node['wildfly']['dom']['host_conf']) do
  source "#{node['wildfly']['dom']['host_conf']}.erb"
  user node['wildfly']['user']
  group node['wildfly']['group']
  mode '0644'
  variables({
    mgmt_int: node['wildfly']['int']['mgmt']['bind'],
    mgmt_http_port: node['wildfly']['int']['mgmt']['http_port'],
    mgmt_native_port: node['wildfly']['int']['mgmt']['native_port'],
    pub_int: node['wildfly']['int']['pub']['bind'],
    host_name: node['wildfly']['dom']['host']['name'],
    remote_host: node['wildfly']['dom']['remote']['host'],
    remote_port: node['wildfly']['dom']['remote']['port'],
    server_heap_size: node['wildfly']['dom']['server']['jvm']['heap']['size'],
    server_heap_max: node['wildfly']['dom']['server']['jvm']['heap']['max'],
    server_permgen_size: node['wildfly']['dom']['server']['jvm']['permgen']['size'],
    server_permgen_max: node['wildfly']['dom']['server']['jvm']['permgen']['max'],
    slave: node['wildfly']['dom']['host']['slave'],
    secret: node['wildfly']['custom']['slave']['secret']
  })
  notifies :restart, "service[#{node['wildfly']['service']}]", :delayed
  only_if { !File.exists?(File.join(node['wildfly']['base'], '.chef_deployed')) || node['wildfly']['enforce_config'] }
end

# => Configure Wildfly CLI
template File.join(node['wildfly']['base'], 'bin', 'jboss-cli.xml') do
  source 'jboss-cli.xml.erb'
  user node['wildfly']['user']
  group node['wildfly']['group']
  mode '0644'
  variables({
    ip: node['ipaddress']
  })
  only_if { !File.exists?(File.join(node['wildfly']['base'], '.chef_deployed')) || node['wildfly']['enforce_config'] }
end

# => Configure Wildfly Domain - MGMT Users
template File.join(node['wildfly']['base'], 'domain', 'configuration', 'mgmt-users.properties') do
  source 'mgmt-users.properties.erb'
  user node['wildfly']['user']
  group node['wildfly']['group']
  mode '0600'
  variables({
    mgmt_users: node['wildfly']['users']['mgmt']
  })
end

# => Configure Java Options
template File.join(node['wildfly']['base'], 'bin', 'domain.conf') do
  source 'domain.conf.erb'
  user node['wildfly']['user']
  group node['wildfly']['group']
  mode '0644'
  variables({
    xms: node['wildfly']['java_opts']['xms'],
    xmx: node['wildfly']['java_opts']['xmx'],
    maxpermsize: node['wildfly']['java_opts']['xx_maxpermsize'],
    preferipv4: node['wildfly']['java_opts']['preferipv4'],
    headless: node['wildfly']['java_opts']['headless']
  })
  only_if { !File.exists?(File.join(node['wildfly']['base'], '.chef_deployed')) || node['wildfly']['enforce_config'] }
end

# Create file to indicate deployment and prevent recurring configuration deployment
file File.join(node['wildfly']['base'], '.chef_deployed') do
  owner node['wildfly']['user']
  group node['wildfly']['group']
  action :create_if_missing
end

# => Start the Wildfly Service
service node['wildfly']['service'] do
  action :start
end
