# encoding: UTF-8
#
# Cookbook Name:: wildfly
# Recipe:: default
#
# Copyright (C) 2014 Brian Dwyer - Intelligent Digital Services
# 
# All rights reserved - Do Not Redistribute
#

node.override['wildfly']['custom']['datasource']['analysis']['pool']['min'] = '0'

include_recipe 'wildfly::install_master'