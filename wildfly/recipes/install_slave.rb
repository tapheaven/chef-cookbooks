# encoding: UTF-8
#
# Cookbook Name:: wildfly
# Recipe:: default
#
# Copyright (C) 2014 Brian Dwyer - Intelligent Digital Services
# 
# All rights reserved - Do Not Redistribute
#

master_layer = node['wildfly']['custom']['opsworks']['layer']['master']

node.override['wildfly']['dom']['host']['slave'] = true
node.override['wildfly']['dom']['host']['name'] = "slave_#{node['hostname']}"
node.override['wildfly']['dom']['remote']['host'] = node['opsworks']['layers'][master_layer]['instances'].values[0]['private_ip']

include_recipe 'wildfly::install'