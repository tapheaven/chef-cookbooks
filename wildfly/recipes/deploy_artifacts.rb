# encoding: UTF-8
#
# Cookbook Name:: wildfly
# Recipe:: deploy_artifacts
#
# Copyright (C) 2014 Brian Dwyer - Intelligent Digital Services
# 
# All rights reserved - Do Not Redistribute
#

include_recipe "s3_file"

node['wildfly']['deploy']['artifacts'].each do |artifact|
	# => Set artifact path
	path = "#{Chef::Config[:file_cache_path]}/#{artifact['name']}"
	
	# => Download artifact from S3
	s3_file path do
	  bucket artifact['s3_bucket']
	  remote_path artifact['s3_path']
	  aws_access_key_id 'AKIAIQFBCIAN6IYHWZEQ'
	  aws_secret_access_key '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'
	  owner "wildfly"
	  group "wildfly"
	  mode 0644
	end
	
	# => Deploy to Wildfly
	wildfly_deploy artifact['name'] do
		path path
		server_groups artifact['server_groups']
	end  
end