# encoding: UTF-8
#
# Cookbook Name:: wildfly
# Recipe:: default
#
# Copyright (C) 2014 Brian Dwyer - Intelligent Digital Services
# 
# All rights reserved - Do Not Redistribute
#

node.override['wildfly']['dom']['host']['slave'] = false
node.override['wildfly']['dom']['host']['name'] = "master_#{node['hostname']}"

include_recipe 'wildfly::install'