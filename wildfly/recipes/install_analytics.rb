# encoding: UTF-8
#
# Cookbook Name:: wildfly
# Recipe:: default
#
# Copyright (C) 2014 Brian Dwyer - Intelligent Digital Services
# 
# All rights reserved - Do Not Redistribute
#

node.override['wildfly']['custom']['datasource']['analysis']['url'] = 'jdbc:postgresql://localhost:6432/tapheaven'
node.override['wildfly']['custom']['datasource']['analysis']['pool']['min'] = '10'
node.override['wildfly']['custom']['datasource']['analysis']['pool']['max'] = '50'

include_recipe 'wildfly::install_master'